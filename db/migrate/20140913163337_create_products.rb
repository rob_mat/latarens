class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :code
      t.string :symbol
      t.string :group
      t.references :category, index: true
      t.string :product_model
      t.string :product_thumbnail
      t.integer :mountings_count
      t.float :total_height
      t.float :inital_pitch_angle
      t.boolean :is_mountable, default: false
      t.text :parts_materials
      t.text :additional_elements
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
