class AddHeightBeloweAboveToProducts < ActiveRecord::Migration
  def change
    add_column :products, :height_below, :float, default: 0.0
    add_column :products, :height_above, :float, default: 0.0
  end
end
