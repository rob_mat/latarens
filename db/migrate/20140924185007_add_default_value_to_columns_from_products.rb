class AddDefaultValueToColumnsFromProducts < ActiveRecord::Migration
  def change
    change_column :products, :group, :string, default: ''
    change_column :products, :mountings_count, :integer, default: 0
    change_column :products, :inital_pitch_angle, :float, default: 0.0
    change_column :products, :is_mountable, :boolean, default: false
  end
end
