class RemoveTotalHeightFromProducts < ActiveRecord::Migration
  def change
    remove_column :products, :total_height, :integer
  end
end
