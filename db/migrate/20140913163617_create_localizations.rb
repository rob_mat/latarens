class CreateLocalizations < ActiveRecord::Migration
  def change
    create_table :localizations do |t|
      t.string :language
      t.references :product, index: true
      t.string :name
      t.text :description
      t.float :price
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
