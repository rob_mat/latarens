class AddTechnicalDrawingFileMameToLocalization < ActiveRecord::Migration
  def change
    add_column :localizations, :technical_drawing_file_name, :string
  end
end
