class AddNewColumnsToProduct < ActiveRecord::Migration
  def change
    add_column :products, :product_type, :string
    add_column :products, :available_anodized_materials_indices, :text
    add_column :products, :default_anodized_color, :string
    add_column :products, :disallowed_connections, :text
  end
end
