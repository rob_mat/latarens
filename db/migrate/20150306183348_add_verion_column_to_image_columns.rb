class AddVerionColumnToImageColumns < ActiveRecord::Migration
  def change
    add_column :categories, :thumbnail_name_version, :integer, default: 0
    add_column :products, :product_model_version, :integer, default: 0
    add_column :products, :product_thumbnail_version, :integer, default: 0
    add_column :localizations, :technical_drawing_file_name_version, :integer, default: 0
  end
end
