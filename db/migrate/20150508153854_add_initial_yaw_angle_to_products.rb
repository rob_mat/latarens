class AddInitialYawAngleToProducts < ActiveRecord::Migration
  def change
    add_column :products, :initial_yaw_angle, :float, default: 0.0
  end
end
