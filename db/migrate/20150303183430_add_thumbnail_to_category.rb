class AddThumbnailToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :thumbnail_name, :string
  end
end
