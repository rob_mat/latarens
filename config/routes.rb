Rails.application.routes.draw do

  resources :categories, only: [:index] do
    member do
      get :products
    end
    collection do
      get :last_update
    end
  end

  resources :products, only: [:show, :index]
  resources :mountings, only: [:index]
  resources :localizations, only: [:index]
  resources :localized_categories, only: [:index]
  resources :additional_elements, only: [:index]
  resources :additional_element_localizations, only: [:index]

  resources :salesmen, only: [] do
    collection do
      post :login
      post :update
    end
  end

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  root 'admin/dashboard#index'
end
