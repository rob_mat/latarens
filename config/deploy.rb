require "bundler/capistrano"
require "rvm/capistrano"

server "127.0.0.1", :web, :app, :db, primary: true

set :application, "latarens"
set :user, "rails"
set :port, 22
set :deploy_to, "/home/#{user}/#{application}"
set :deploy_via, :clone
set :use_sudo, false

set :scm, "git"
set :repository, "."
set :branch, "master"

set :resque_environment_task, true

role :resque_worker, "5.196.9.177"
role :resque_scheduler, "5.196.9.177"
set :resque_log_file, "log/resque.log"
set :workers, { "sloth" => 1 }

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

after "deploy", "deploy:cleanup" # keep only the last 5 releases

namespace :deploy do
  %w[start stop restart].each do |command|
    desc "#{command} unicorn server"
    task command, roles: :app, except: {no_release: true} do
      run "/etc/init.d/unicorn_#{application} #{command}"
    end
  end

  task :setup_config, roles: :app do
    sudo "ln -nfs #{current_path}/config/nginx.conf /etc/nginx/sites-enabled/#{application}"
    sudo "ln -nfs #{current_path}/config/unicorn_init.sh /etc/init.d/unicorn_#{application}"
    run "mkdir -p #{shared_path}/config"
    # put File.read("config/database.example.yml"), "#{shared_path}/config/database.yml"
    puts "Now edit the config files in #{shared_path}."
  end
  after "deploy:setup", "deploy:setup_config"

  task :symlink_config, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "rm -rf #{release_path}/config/databases && ln -nfs #{shared_path}/config/databases #{release_path}/config/"
    run "ln -nfs #{shared_path}/config/secrets.yml #{release_path}/config/secrets.yml"
  end
  after "deploy:finalize_update", "deploy:symlink_config"

end

require './config/boot'
# require 'airbrake/capistrano'
