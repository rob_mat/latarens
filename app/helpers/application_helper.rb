module ApplicationHelper

  def full_url url
    [request.protocol, request.host_with_port, url].join
  end
end
