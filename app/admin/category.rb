ActiveAdmin.register Category do
  decorate_with CategoryDecorator
  
  form do |f|
    f.semantic_errors # shows errors on :base
    f.inputs do
      f.input :identifier
      f.input :name
      f.input :parent, collection: Category.where.not(id: f.object.self_and_descendants.all.map(&:id)) 
      f.input :thumbnail_name, as: :file, hint: f.object.errors[:thumbnail_name].blank? && f.object.thumbnail_name
      f.input :thumbnail_name_cache, as: :hidden
    end
    f.actions         # adds the 'Submit' and 'Cancel' buttons
  end

  permit_params :name, :parent_id, :identifier, :thumbnail_name, :thumbnail_name_cache, localized_categories_attributes: [:language, :name]

  index do
    selectable_column
    column :identifier
    column :name
    column :parent
    column :subcategories 
    column :created_at
    actions defaults: true do |category|
    end
  end

  show do |category|
    attributes_table do
      row :identifier
      row :name
      row :parent
      row :thumbnail_name
      row :subcategories 
      row :created_at
      row :updated_at
    end
  end

  filter :identifier
  filter :parent
  filter :subcategories
  filter :created_at
  filter :updated_at

end
