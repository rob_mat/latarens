ActiveAdmin.register Localization do
  belongs_to :product
  decorate_with LocalizationDecorator
  navigation_menu :product

  permit_params :language, :name, :description, :price, :technical_drawing_file_name, :technical_drawing_file_name_cache 

  form do |f|
    f.inputs do
      f.input :language, collection: Localization::LOCALIZATION_CODE, include_blank: false
      f.input :name
      f.input :price
      f.input :technical_drawing_file_name, as: :file
      f.input :technical_drawing_file_name_cache, as: :hidden
      f.input :description
    end
    f.actions
  end

  index do
    selectable_column
    column :language
    column :name
    column :price
    column :created_at 
    actions
  end

  show do |category|
    attributes_table do
      row :language
      row :name
      row :price
      row :technical_drawing_file_name
      row :description
      row :created_at 
      row :updated_at 
    end
  end

  filter :language 
  filter :name
  filter :description 
  filter :price
  filter :created_at

end 
