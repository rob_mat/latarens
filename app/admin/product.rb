ActiveAdmin.register Product do
  include Imports
  decorate_with ProductDecorator
  permit_params :code, :category_id, :product_model, :product_thumbnail, :mountings_count, :height_above, :height_below, :inital_pitch_angle, :initial_yaw_angle, :additional_elements_array, :symbol, :is_mountable, :group, :product_model_cache, :product_thumbnail_cache, :product_type, :available_anodized_materials_indices, :default_anodized_color, :disallowed_connections_array, :disallowed_connections, localizations_attributes: [:language, :name, :description, :technical_drawing_file_name, :price]

  form do |f|
    f.semantic_errors :localizations
    f.inputs Product.model_name.human(count: 1), :multipart => true do
      f.input :code
      f.input :symbol
      f.input :group
      f.input :category, collection: Category.leaves.all.map{ |k| ["#{k.identifier} - #{k.name}", k.id]}
      f.input :product_type, collection: Product::PRODUCT_TYPE
      f.input :product_model, as: :file
      f.input :product_model_cache, as: :hidden
      f.input :product_thumbnail, as: :file, hint: f.object.errors[:product_thumbnail].blank? && f.object.product_thumbnail
      f.input :product_thumbnail_cache, as: :hidden
      f.input :mountings_count 
      f.input :height_above
      f.input :height_below
      f.input :inital_pitch_angle 
      f.input :initial_yaw_angle 
      f.input :is_mountable, as: :select, collection: [['Nie', false], ['Tak', true]], include_blank: false
      # f.input :parts_materials, as: :string 
      f.input :available_anodized_materials_indices, as: :string 
      f.input :default_anodized_color, collection: Product::DEFAULT_ANODIZED_COLOR 
    end
    if f.object.new_record?
      f.semantic_errors :localizations
      f.inputs Localization.model_name.human(count: 2) do
        f.has_many :localizations, allow_destroy: true, heading: nil do |p|
          p.input :language, collection: Localization::LOCALIZATION_CODE, include_blank: false
          p.input :name
          p.input :price
          p.input :technical_drawing_file_name, as: :file
          p.input :technical_drawing_file_name_cache, as: :hidden
          p.input :description
        end
      end
    end
    f.actions
  end

  index do |product|
    selectable_column
    column :code
    column :symbol
    column :group
    column :category
    column :mountings_count
    column :created_at
    actions defaults: true do |product|
      (link_to Localization.model_name.human(count: 2), admin_product_localizations_path(product.id), class: 'member_link') 
    end
  end

  show do |product|
    attributes_table do
      row :code
      row :symbol
      row :group
      row :category
      row :product_type
      row :product_model
      row :product_thumbnail
      row :mountings_count 
      row :height_above
      row :height_below
      row :inital_pitch_angle 
      row :initial_yaw_angle 
      row :is_mountable 
      # row :parts_materials 
      row :available_anodized_materials_indices
      row :default_anodized_color 
      row :additional_elements 
      row :disallowed_connections 
      row :created_at 
      row :updated_at 
    end
  end

  filter :code 
  filter :symbol
  filter :group
  filter :category 
  filter :product_type, as: :check_boxes, collection: Product::PRODUCT_TYPE
  filter :mountings_count 
  filter :height_above
  filter :height_below
  filter :inital_pitch_angle 
  filter :initial_yaw_angle 
  filter :is_mountable 
  # filter :parts_materials 
  filter :additional_elements 
  filter :default_anodized_color, as: :check_boxes, collection: Product::DEFAULT_ANODIZED_COLOR
  filter :created_at

  sidebar "Elementy", only: [:show, :edit] do
    ul do
      li link_to Localization.model_name.human(count: 2), admin_product_localizations_path(product)
    end
  end

  collection_action :import_prices_csv, :method => :get do
    @page_title = I18n.t('active_admin.import_prices_csv.title')
    @product_prices_import = ProductPricesImport.new
  end

  collection_action :import_prices, :method => :post do
    @page_title = I18n.t('active_admin.import_prices_csv.title')
    product_prices_import = ProductPricesImport.new(plik: params[:file])
    if product_prices_import.save
      redirect_to admin_products_path, notice: 'Import nowych cen zakończony sukcesem!'
    else
      flash.now[:error] = product_prices_import.errors.full_messages.join('<br/>').html_safe
      render 'import_prices_csv'
    end
  end

  collection_action :import, :method => :get do
    @page_title = I18n.t('active_admin.products_import.title')
    @products_import = ProductsImport.new
  end

  collection_action :import_save, :method => :post do
    @page_title = I18n.t('active_admin.products_import.title')
    products_import = ProductsImport.new(plik: params[:plik], archiwum_zdjec: params[:archiwum_zdjec])
    if products_import.save
      redirect_to admin_products_path, notice: 'Import produktów zakończony sukcesem!'
    else
      flash.now[:error] = products_import.errors.full_messages.join('<br/>').html_safe
      render 'import'
    end
  end

  collection_action :import_localizations, :method => :get do
    @page_title = I18n.t('active_admin.localizations_import.title')
    @localizations_import = LocalizationsImport.new
    render 'admin/localizations/import'
  end

  collection_action :import_localizations_save, :method => :post do
    @page_title = I18n.t('active_admin.localizations_import.title')
    localizations_import = LocalizationsImport.new(plik: params[:plik], archiwum_zdjec: params[:archiwum_zdjec])
    if localizations_import.save
      redirect_to admin_products_path, notice: 'Import lokalizacji produktów zakończony sukcesem!'
    else
      flash.now[:error] = localizations_import.errors.full_messages.join('<br/>').html_safe
      render 'admin/localizations/import'
    end
  end

  collection_action :codes_and_groups, :method => :get do
    render json: Product.codes_and_groups(params[:q]) 
  end         
end
