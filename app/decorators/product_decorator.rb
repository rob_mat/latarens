class ProductDecorator < Draper::Decorator
  delegate_all

  def product_thumbnail
    h.image_tag object.product_thumbnail.url
  end

  def product_model
    h.link_to object.product_model.url, object.product_model.url, target: '_blank'
  end

  def category
    object.category && object.category.self_and_ancestors.all.map do |sc|
      h.link_to sc.name, h.admin_category_path(sc)
    end.join(' -> ').html_safe || 'Brak'
  end

  def is_mountable
    !object.is_mountable.nil? && I18n.t(object.is_mountable.to_s)
  end

  def category_total
    object.category && object.category.self_and_ancestors.all.map do |sc|
      sc.name
    end.join('/').html_safe || 'Brak'
  end

end
