class CategoryDecorator < Draper::Decorator
  delegate_all

  def parent
    if object.parent.present?
      object.ancestors.all.map do |sc|
        h.link_to sc.name, h.admin_category_path(sc)
      end.join(' -> ').html_safe
    else
      h.content_tag :span, 'Brak', class: 'empty' 
    end
  end

  def subcategories
    if object.descendants.any?
      object.descendants.all.map do |sc|
        h.link_to sc.name, h.admin_category_path(sc)
      end.join(', ').html_safe
    else
      h.content_tag :span, 'Brak', class: 'empty' 
    end
  end

  def thumbnail_name
    h.image_tag object.thumbnail_name.url
  end

end
