class LocalizationDecorator < Draper::Decorator
  delegate_all

  def technical_drawing_file_name
    h.link_to object.technical_drawing_file_name.filename, object.technical_drawing_file_name.url, target: '_blank'
  end

  def description
    h.simple_format object.description
  end
end
