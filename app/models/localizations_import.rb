class LocalizationsImport
  include CsvImportable
  include ZipImportable

  def self.humanized_attributes 
    { archiwum_zdjec: 'Archiwum Zip z plikami',
      plik: 'Plik CSV' }
  end
  
  def load_imported
    read_csv do |row|
      localization = Localization.new
      localization.import_mode = true
      localization.product = Product.find_by(code: row['code'])
      localization.language = row['language']
      localization.description = row['description']
      localization.name = row['name']
      localization.price = row['price']
      localization.technical_drawing_file_name = read_from_zip_file(row['technicalDrawingFileName'])
      localization
    end
  end

  def update_mode?
    false
  end

  def valid_import?
    true
  end
end
