require 'zip'
module ZipImportable
  extend ActiveSupport::Concern
  # musi implelemować metody:
  # load_imported -> która zwraca elementy do zapisania będź zmiany
  # update_mode? -> która zwraca true lub false gdy chodzi o zmianę

  included do
    attr_accessor :archiwum_zdjec

    validates :archiwum_zdjec, presence: true
    validate :plik_type_zip
  end

  def initialize(attributes = {})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def read_from_zip_file(file_name)
    return nil unless file_name

    Zip::File.open(archiwum_zdjec.path) do |zip_file|
      entry = zip_file.glob(file_name).first
      if entry 
        filename = entry.name
        basename = File.basename(filename)
        tempfile = Tempfile.new(basename)
        tempfile.binmode
        tempfile.write entry.get_input_stream.read
        return ActionDispatch::Http::UploadedFile.new(tempfile: tempfile, filename: basename)
      end
    end
  end

  def save
    if valid? && valid_import?
      if imported.map(&:valid?).all?
        imported.each(&:save!)
        return true
      else
        imported.each_with_index do |item, index|
          if item.new_record? && update_mode?
            errors.add :base, "Wiersz #{index+1}: element który ma być zmieniony nie insteje.\n"
          else
            item.errors.full_messages.each do |message|
              errors.add :base, "Wiersz #{index+1}: #{message}\n"
            end
            return false
          end
        end
        return false
      end
    else
      return false
    end
  end

  module ClassMethods

  end

  protected

  def plik_type_zip
    if archiwum_zdjec && File.extname(archiwum_zdjec.original_filename) != '.zip'
      errors[:archiwum_zdjec] << I18n.t('activerecord.errors.plik_type_zip')
    end
  end

end

