module CsvImportable
  extend ActiveSupport::Concern
  # musi implelemować metody:
  # load_imported -> która zwraca elementy do zapisania będź zmiany
  # update_mode? -> która zwraca true lub false gdy chodzi o zmianę

  included do
    extend ActiveModel::Naming
    include ActiveModel::Translation 
    include ActiveModel::Conversion
    include ActiveModel::Validations

    attr_accessor :plik

    validates :plik, presence: true
    validate :plik_type

    def self.human_attribute_name(attr, options = {})
      humanized_attributes[attr.to_sym] || super
    end

  end

  def initialize(attributes = {})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def save
    if valid? && valid_import?
      if imported.map(&:valid?).all?
        imported.each(&:save!)
        return true
      else
        imported.each_with_index do |item, index|
          if item.new_record? && update_mode?
            errors.add :base, "Wiersz #{index+1}: element który ma być zmieniony nie insteje.\n"
          else
            item.errors.full_messages.each do |message|
              errors.add :base, "Wiersz #{index+1}: #{message}\n"
            end
            return false
          end
        end
        return false
      end
    else
      return false
    end
  end

  def imported
    @imported ||= load_imported
  end

  def read_csv
    localizations = []
    CSV.foreach(plik.path, { headers: true }) do |row|
      localizations << yield(row)
    end
    localizations
  end

  module ClassMethods

  end

  protected

  def plik_type
    if plik && File.extname(plik.original_filename) != '.csv'
      errors[:plik] << I18n.t('activerecord.errors.plik_type')
    end
  end
end
