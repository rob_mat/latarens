class Localization < ActiveRecord::Base
  acts_as_paranoid
  belongs_to :product

  attr_accessor :import_mode, :request_type
  
  LOCALIZATION_CODE = %w{ PL EN RU }

  mount_uploader :technical_drawing_file_name, LocalizationTechnicalDrawingFileNameUploader
  validates :language, presence: true, uniqueness: { scope: [:product_id, :deleted_at] }, inclusion: { in: LOCALIZATION_CODE }
  validates :name, presence: true, uniqueness: { scope: [:product_id, :deleted_at] }
  validates :description, presence: true
  validates :price, presence: true, numericality: true
  validates :product, presence: { message: 'nie istnieje' }, if: 'import_mode'
  validates :technical_drawing_file_name,
    :presence => true, 
    :file_size => { 
      :maximum => 3.megabytes.to_i 
    }
  validate :validate_price_gt_zero
  validate :validate_file_name_is_unique

  delegate :code, to: :product

  def product_code
    self.product.try(:code)
  end

  def technical_drawing_file_name=(obj)
    super(obj)
    self.technical_drawing_file_name_version += 1
  end

  def self.find_by_conde_and_language(code, language)
    joins(:product).where(products: { code: code }, language: language).first
  end

  def request_type
    @request_type || 'admin'
  end

  protected

  def validate_price_gt_zero
    if self.price && self.price <= 0
      self.errors[:price] << 'musi być większe od zera'
    end
  end

  def validate_file_name_is_unique
    [:technical_drawing_file_name].each do |img|
      if send(img).filename && self.class.where(img => self.send(img).filename, deleted_at: nil).count > 0
        errors.add img, "'#{send(img).filename}' ta nazaw już istnieje"
      end
    end
  end

end
