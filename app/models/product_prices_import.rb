class ProductPricesImport
  include CsvImportable

  def self.humanized_attributes 
    { plik: 'Plik CSV' }
  end
  
  def load_imported
    read_csv do |row|
      localization =  Localization.find_by_conde_and_language(row['code'], row['language']) || Localization.new
      localization.price = row['price']
      localization
    end
  end

  def update_mode?
    true
  end

  def valid_import?
    true
  end
end
