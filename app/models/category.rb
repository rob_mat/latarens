class Category < ActiveRecord::Base
  acts_as_paranoid
  acts_as_nested_set

  attr_accessor :category_thumbnail_height, :category_thumbnail_width, :request_type
  attr_reader :parent_identifier

  has_many :products

  mount_uploader :thumbnail_name, CategoryThumbnailUploader
  validates :identifier, presence: true, uniqueness: { :scope => :deleted_at }
  validates :name, presence: true, uniqueness: { :scope => :deleted_at } 
  validates :thumbnail_name, 
    presence: true,
    file_size: { 
      :maximum => 100.kilobytes.to_i 
    }
  validate :validate_category_thumbnail_dimentions

  def to_s
    "#{identifier} - #{name}"
  end

  def parent_identifier
    self.parent.try(:identifier)
  end

  def thumbnail_name=(obj)
    super(obj)
    self.thumbnail_name_version += 1
  end

  def request_type
    @request_type || 'admin'
  end

  protected 

  def validate_category_thumbnail_dimentions
    if self.new_record? && 
      self.thumbnail_name && 
      self.thumbnail_name.get_dimensions != [120, 120] && 
      self.category_thumbnail_width
      self.errors[:thumbnail_name] << I18n.t('errors.messages.wrong_dimentions', width: self.category_thumbnail_width, height: self.category_thumbnail_height)
    end
  end

end
