class Product < ActiveRecord::Base
  acts_as_paranoid

  PRODUCT_TYPE = %w{ KINKIET KLOSZ KOLUMNA OPRAWA RASTER SŁUP SŁUP_BEZ_WNĘKI UKŁAD_RAMION WYSIĘGNIK ZESTAW }
  DEFAULT_ANODIZED_COLOR = ['C-0', 'C-32', 'CI-63', 'CI-65', 'C-33', 'C-45', 'C-34', 'C-35', 'CI-78', 'CI-75', 'C-0W', 'C-32W', 'CI-63W', 'CI-65W', 'C-33W', 'C-45W', 'C-34W', 'C-35W', 'CI-78W', 'CI-75W']

  attr_accessor :product_thumbnail_height, :product_thumbnail_width, :request_type
  attr_writer :skip
  
  belongs_to :category
  has_many :localizations, dependent: :destroy

  accepts_nested_attributes_for :localizations
  mount_uploader :product_model, ProductModelUploader
  mount_uploader :product_thumbnail, ProductThumbnailUploader
  skip_callback :save, :before, :store_picture!, if: 'import_mode'

  validates :code, presence: true, uniqueness: { :scope => :deleted_at }
  validates :symbol, presence: true, uniqueness: { :scope => :deleted_at }
  validates :category_id, presence: true
  validates :product_thumbnail,
    :presence => true, on: :create
  validates :product_thumbnail,
    :file_size => { 
      :maximum => 100.kilobytes.to_i 
    }
  validates :product_model, presence: true
  validate :validate_file_name_is_unique
  
  validate :validate_product_thumbnail_dimentions
  validates :localizations, presence: true, unless: 'import_mode?'
  validates :product_type, presence: true, inclusion: { in: PRODUCT_TYPE }
  validates :default_anodized_color, inclusion: { in: DEFAULT_ANODIZED_COLOR }, allow_nil: true

  validates :additional_elements, format: /\A;?(\[([^\]\[;]+)\];)*\[[^\]\[;]+\];?\z/, allow_blank: true 
  validate :additional_elements_code_presence
  validate :additional_elements_code_uniqueness

  validates :disallowed_connections, format: /\A;?(\[([^\]\[;]+)\];)*\[[^\]\[;]+\];?\z/, :allow_blank => true

  after_validation do |e|
    self.errors[:additional_elements_array] = self.errors[:additional_elements] 
    self.errors[:disallowed_connections_array] = self.errors[:disallowed_connections] 
  end

  def additional_elements_array                                    
    if self.additional_elements
      aca = self.additional_elements.split(';').delete_if(&:blank?).map do |elem| 
        e = elem[1..-2] 
        { id: e, name: e }
      end
    end
  end

  def additional_elements_array=(ac_array)
    self.additional_elements = ac_array.split(',').map { |e| "[#{e}]" }.join(';')
  end

  def disallowed_connections_array                                    
    if self.disallowed_connections
      aca = self.disallowed_connections.split(';').delete_if(&:blank?).map do |elem| 
        e = elem[1..-2] 
        { id: e, name: e }
      end
    end
  end

  def disallowed_connections_array=(ac_array)
    self.disallowed_connections = ac_array.split(',').map { |e| "[#{e}]" }.join(';')
  end

  def add_new_connection(new_elem)
    if self.additional_elements.present?
      self.additional_elements += ";#{new_elem}"
    else
      self.additional_elements = new_elem
    end
  end

  def category_identifier
    self.category.try(:identifier)
  end

  def product_model=(obj)
    super(obj)
    self.product_model_version += 1
  end

  def product_thumbnail=(obj)
    super(obj)
    self.product_thumbnail_version += 1
  end

  def to_s
    "PRODUKT: #{self.code}"
  end

  def self.import(csv_file)
    true
  end

  def skip
     @skip || false
  end

  def import_mode?
    @skip
  end

  def request_type
    @request_type || 'admin'
  end

  def self.codes_and_groups(code)
    codes = (where("LOWER(code) like ?", "#{code}%").pluck(:code) + 
     where("LOWER(products.group) like ?", "#{code}%").pluck(:group)
    ).compact
    if codes.any?
      codes.map { |e| {id: e, name: e} }
    end
  end

  protected 

  def validate_product_thumbnail_dimentions
    if self.new_record? && 
      self.product_thumbnail && 
      self.product_thumbnail.get_dimensions != [120, 120] && 
      self.product_thumbnail_width
      self.errors[:product_thumbnail] << I18n.t('errors.messages.wrong_dimentions', width: self.product_thumbnail_width, height: self.product_thumbnail_height)
    end
  end

  def validate_file_name_is_unique
    [:product_thumbnail, :product_model].each do |img|
      if send(img).filename && self.class.where(img => self.send(img).filename, deleted_at: nil).count > 0
        errors.add img, "'#{send(img).filename}' ta nazaw już istnieje"
      end
    end
  end

  def additional_elements_code_presence
    if !self.import_mode? && self.additional_elements 
      codes = self.additional_elements.split(';').map{ |elem| elem[1..-2] }.uniq
      codes.each do |code|
        unless AdditionalElement.where('code = :code', code: code).any?
          self.errors[:additional_elements] << "brak elemntu dodatkowego o kodzie #{code}"
        end
      end
    end
  end

  def additional_elements_code_uniqueness
    if !self.import_mode? && self.additional_elements && self.additional_elements_array.uniq.count != self.additional_elements_array.count
      self.errors[:additional_elements] << "na liście występują powtarzające się elementy"
    end
  end

 
end
