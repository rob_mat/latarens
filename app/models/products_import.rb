require 'zip'
class ProductsImport
  include CsvImportable
  include ZipImportable

  def self.humanized_attributes 
    {
      archiwum_zdjec: 'Archiwum Zip z plikami',
      plik: 'Plik CSV'
    }
  end
  
  def load_imported
    read_csv do |row|
      produkt =  Product.new
      produkt.skip = true
      produkt.code = row['code']
      produkt.symbol = row['symbol']
      produkt.category = Category.find_by(identifier: row['category'])
      produkt.group = row['group']
      produkt.product_model = read_from_zip_file(row['modelFileName'])
      produkt.parts_materials = row['partsMaterials']
      produkt.product_thumbnail = read_from_zip_file(row['thumbnailName'])
      produkt.height_above = row['heightAboveMounting']
      produkt.height_below = row['heightBelowMounting']
      produkt.mountings_count = row['mountingsCount']
      produkt.is_mountable = (row['isMountable'] == 'YES')
      produkt.inital_pitch_angle = row['initialPitchAngle'].present? ? row['initialPitchAngle'] : 0.0
      produkt.initial_yaw_angle = row['initialYawAngle'].present? ? row['initialYawAngle'] : 0.0
      produkt.additional_elements = row['additionalElements']
      produkt.product_type = row['type']
      produkt.available_anodized_materials_indices = row['availableAnodizedMaterialsIndices']
      produkt.default_anodized_color = row['defaultAnodizedColor']
      produkt.disallowed_connections = row['disallowedConnections']
      produkt
    end
  end

  def update_mode?
    false
  end

  def valid_import?

    all_products_model = []
    all_products_thumbnail = []
    return_val = true
    CSV.foreach(plik.path, { headers: true }) do |row|
      all_products_model << row['modelFileName']
      all_products_thumbnail << row['thumbnailName']
    end
    
    if all_products_model.count != all_products_model.uniq.count
       errors.add :base, "Plik CSV nie zawiera unikalnych nazw plików modelu."
       return_val = false
    end
    if all_products_thumbnail.count != all_products_thumbnail.uniq.count
       errors.add :base, "Plik CSV nie zawiera unikalnych nazw plików miniaturek."
       return_val = false
    end
    return return_val
  end

end
