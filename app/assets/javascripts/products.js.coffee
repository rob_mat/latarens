# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  
  $('#product_additional_elements_array').tokenInput $('#product_additional_elements_array').data('url'),
    theme: 'facebook'
    prePopulate: $('#product_additional_elements_array').data('load')
    minChars: 2
    hintText: 'Wpisz początek aby wyszukać'
    noResultsText: 'Brak wyników'                        
    searchingText: 'Wyszukuję...'

