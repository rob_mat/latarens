json.products @products do |product|
  json.id product.id
  json.code product.code
  json.symbol product.symbol
  json.group product.group
  json.category_id product.category_id
  json.category_name product.category.name
end
json.total_pages @products.total_pages
json.current_page @products.current_page
