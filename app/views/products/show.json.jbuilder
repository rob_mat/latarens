json.id @product.id
json.code @product.code
json.symbol @product.symbol
json.group @product.group
json.full_category @product.decorate.category_total
json.full_category_ids @product.category.self_and_ancestors.map(&:id)
json.product_model_url full_url(@product.product_model.url)
json.product_thumbnail_url full_url(@product.product_thumbnail.url)
json.mountings_count @product.mountings_count
json.height_above @product.height_above
json.height_below @product.height_below
json.inital_pitch_angle @product.inital_pitch_angle
json.is_mountable @product.is_mountable
json.parts_materials @product.parts_materials
json.additional_elements @product.additional_elements
json.created_at l @product.created_at
json.updated_at l @product.updated_at
json.localizations_numer @product.localizations.count
json.mountings_number @product.mountings.count
