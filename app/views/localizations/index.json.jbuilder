json.localizations @product.localizations do |localization|
  json.id localization.id
  json.language localization.language
  json.name localization.name
  json.description localization.description
  json.price localization.price
  json.created_at l(localization.created_at)
  json.updated_at l(localization.updated_at)
end
