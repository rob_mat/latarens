json.id category.identifier
json.name category.name
json.thumbnail_name_url category.thumbnail_name ? full_url(category.thumbnail_name.url) : ''
json.created_at l(category.created_at)
if category.children.any?
  json.subcategories category.children do  |category|
    json.partial! 'category', category: category
  end
end
