json.categories @categories do |category|
  json.partial! 'category', category: category
  json.subcategories category.children do  |category|
    json.partial! 'category', category: category
  end
end
